let config = require("./config");
let express = require("express");
require('express-async-errors');
let bodyParser = require("body-parser");
let mongoose = require('mongoose');

mongoose.Promise = Promise;

/* istanbul ignore next */
if (config.DATABASEURL !== false) {
    mongoose.connect(config.DATABASEURL, {useNewUrlParser: true});

    mongoose.connection.on('error', async (e) => {
        console.log(e);
    });

    mongoose.connection.once('open', () => {
        console.log(`MongoDB successfully connected`);
    });
}

let app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use("/courses", require("./routes/courses"));
app.use("/vote", require("./routes/vote"));


/* istanbul ignore next */
if (config.PORT !== false) {
    app.listen(config.PORT, _ => console.log(`Server is running on port ${config.PORT}`));
}

module.exports = app;
