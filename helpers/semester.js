/**
 * @param {Date} date 
 */
function fromDate(date){
    if(date.getMonth()==0){
        return date.getFullYear()-1 + "_Fall"
    }
    if (date.getMonth()<=5){
        return date.getFullYear() + "_Spring"
    }
    return date.getFullYear() + "_Fall"
}


function toInternalNumber(semester){
    if(/^\d{4}_(Fall|Spring)$/.test(semester) === false){
        throw new Error("Has to be of format \d{4}_(Fall|Spring), was "+semester)
    }
    semester = semester
        .replace("_Fall", ".5")
        .replace("_Spring", "");

    return parseFloat(semester);
}

function getOldSemesters(allCourses, date){
    date = date || new Date();
    let internalDate = toInternalNumber(fromDate(date));

    return allCourses.filter(i => {
        try {
           return  toInternalNumber(i.Semester) <= internalDate - 3
        }catch (e) {
            throw new Error(e.message + " for course " + i._id)
        }
    });
}

module.exports = {fromDate, toInternalNumber, getOldSemesters}