module.exports = {
    PORT: parseInt(process.env.PORT || 80),
    DATABASEURL: process.env.DATABASEURL || false
};