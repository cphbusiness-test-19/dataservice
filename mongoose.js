const mongoose = require('mongoose');


let models = {
    Course: {
        CourseName: String,
        Room: String,
        TeacherId: Number,
        StudentsId: Array,
        Category: String,
        NoHoursPrWeek: Number,
        Weekdays: String,
        Description: String,
        Semester: String,
        Language: String,
        Level: String,
        Visible: Boolean,
        Archived: Boolean,
        Votes: {type: Number, default: 0}

    },
    AdminUser: {
        Name: String,
        Password: String,
        UserName: String,
        Email: String,
        Phone: String,
        Public: Boolean,
        Active: Number,
        Title: String,
        Token: String,
        Registered: Array,
    },
    Student: {
        Name: String,
    },
    Teacher: {
        Name: String,
        Candidate: Boolean,
    },
    Votes: {
        CourseIds: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Course' }],
        Active: Boolean,
        CreatedAt: {type: Date, default: Date.now}
    }
};


for (let model of Object.keys(models)) {
    mongoose.model(model, models[model]);
}