const chai = require('chai'),
    chaiHttp = require('chai-http'),
    config = require("../config");
var itParam = require('mocha-param');
var MockDate = require('mockdate');

var expect = chai.expect;
const Joi = require('@hapi/joi');


chai.use(chaiHttp);

chai.should();

config.port = false;
let app = require("../server");


describe("votes", function(){
    let voteId, randomCourses;
    describe("create a new vote", function(){
        it("success", async function(){
            let allCourses = await chai.request(app).get("/courses").send();
            randomCourses = allCourses.body.filter(i => !i.Archived).filter((_, index) => index < 3 || Math.random()>.5).map(i => i.Id)

            let newVoting = await chai.request(app).post("/vote").send({
                CourseIds: randomCourses
            });

            expect(newVoting).to.have.status(200);
            expect(newVoting.body.Id).to.be.a("string");


            voteId = newVoting.body.Id;
        })

        it("fails - invalid mongo id", async function(){
            let newVoting = await chai.request(app).post("/vote").send({
                CourseIds: ["0"]
            });

            expect(newVoting).to.have.status(400);
        })
    })

    describe("get vote", function(){
        it("get all votes", async function(){
            let allVote = await chai.request(app).get("/vote").send();
 
            expect(allVote).to.have.status(200);

            const result = Joi.validate(allVote.body, Joi.array().items(Joi.object().keys({
                Id : Joi.string().required(),
                Active : Joi.boolean().required(),
                CreatedAt : Joi.string().required()                
            })));
            expect(result.error).to.be.null;

            expect(allVote.body.find(i=>i.Id === voteId)).to.be.a("object");
        })

        it("get a single vote", async function(){
            let vote =  await chai.request(app).get("/vote/"+voteId).send();
 
            expect(vote).to.have.status(200);

            const result = Joi.validate(vote.body, Joi.object().keys({
                Id : Joi.string().required(),
                Active : Joi.boolean().required(),
                CreatedAt : Joi.string().required(),
                Courses: Joi.array().items(Joi.object().keys({
                    Id: Joi.string().required(),
                    Name: Joi.string().required(),
                    Votes: Joi.number().required(),
                    Category: Joi.string().required(),
                })).required()               
            }));
            expect(result.error).to.be.null;
        });

        it("get a single vote - not found", async function(){
            let vote =  await chai.request(app).get("/vote/000000000000000000000000").send();
 
            expect(vote).to.have.status(404);
            vote.body.error.should.be.eql("Not found");
        })

        it("get a single vote - invalid", async function(){
            let vote =  await chai.request(app).get("/vote/abcde").send();
 
            expect(vote).to.have.status(400);
            vote.body.error.should.be.a("string");
        })
    })

    describe("add vote", function (){
        it("inserts a vote", async function (){
            let vote =  await chai.request(app).get("/vote/"+voteId).send();
            let course1 = vote.body.Courses[0]
            let course2 = vote.body.Courses[1]
            let course3 = vote.body.Courses[3]
 
            let addVote = await chai.request(app).post("/vote/addVotes").send({
                CourseIds: [course1.Id, course2.Id]
            })

            expect(addVote).to.have.status(202)

            let after =  await chai.request(app).get("/vote/"+voteId).send();


            after.body.Courses[0].Votes.should.be.equal(parseInt(course1.Votes) + 1)
            after.body.Courses[1].Votes.should.be.equal(parseInt(course2.Votes) + 1)
            after.body.Courses[2].Votes.should.be.equal(course3.Votes)
        })

        it("fails because of wrong format", async function (){
            let addVote = await chai.request(app).post("/vote/addVotes").send({
                CourseIds: ["wedfg"]
            })

            expect(addVote).to.have.status(400);
            addVote.body.error.should.be.a("string");
        })
    })

    describe("disable vote", function(){
        it("success", async function(){
            let vote = await chai.request(app).get("/vote/"+voteId).send();
            expect(vote).to.have.status(200);
            expect(vote.body.Active).to.be.true;


            let disable = await chai.request(app).patch("/vote/"+voteId+"/disable").send()
            expect(disable).to.have.status(202);


            let afterEditVote = await chai.request(app).get("/vote/"+voteId).send();
            expect(afterEditVote).to.have.status(200);
            expect(afterEditVote.body.Active).to.be.false;
        })

        it("get a single vote - not found", async function(){
            let disable = await chai.request(app).patch("/vote/000000000000000000000000/disable").send()

            expect(disable).to.have.status(404);
            disable.body.error.should.be.eql("Not found");
        })

        it("get a single vote - invalid", async function(){
            let disable =  await chai.request(app).patch("/vote/abcde/disable").send();
 
            expect(disable).to.have.status(400);
            disable.body.error.should.be.a("string");
        })
    })
})