const chai = require('chai');
var itParam = require('mocha-param');
var MockDate = require('mockdate');

var expect = chai.expect;

chai.should();

let semester = require("../helpers/semester")



describe("Course", function () {

    describe('delete archive', function (){
        let baseDate = new Date(2019, 8, 15);
        let template = [{
            Semester: "2015_Spring",
            Keep: 0
        },{
            Semester: "2015_Fall",
            Keep: 0
        },{
            Semester: "2016_Spring",
            Keep: 0
        },{
            Semester: "2016_Fall",
            Keep: 0
        },{
            Semester: "2017_Spring",
            Keep: 1
        },{
            Semester: "2017_Fall",
            Keep: 1
        },{
            Semester: "2018_Spring",
            Keep: 1
        },{
            Semester: "2018_Fall",
            Keep: 1
        },{
            Semester: "2019_Spring",
            Keep: 1
        },{
            Semester: "2019_Fall",
            Keep: 1
        },];

        let dateAndSemester = [
            [new Date(2019, 1 - 1, 31), "2018_Fall", 2018.5],
            [new Date(2019, 2 - 1, 1), "2019_Spring", 2019],
            [new Date(2019, 6 - 1, 30), "2019_Spring", 2019],
            [new Date(2019, 7 - 1, 1), "2019_Fall", 2019.5],
            [new Date(2020, 1 - 1, 31), "2019_Fall", 2019.5],
        ];

        itParam("date to semester ${value}", dateAndSemester,function([date, expected, _]){
            semester.fromDate(date).should.be.equal(expected)
        });

        itParam("semester to internal number ${value}", dateAndSemester,function([_, semesterString, expected]){
            semester.toInternalNumber(semesterString).should.be.equal(expected)
        });

        it("to internal takes right format", function(){
            (function () {
                semester.toInternalNumber("2018Fall")
            }).should.throw(Error,"Has to be of format \d{4}_(Fall|Spring)");
            (function () {
                semester.toInternalNumber("18_Fall")
            }).should.throw(Error,"Has to be of format \d{4}_(Fall|Spring)");
            (function () {
                semester.toInternalNumber("2018_fall")
            }).should.throw(Error,"Has to be of format \d{4}_(Fall|Spring)");
            (function () {
                semester.toInternalNumber("rtghjm")
            }).should.throw(Error,"Has to be of format \d{4}_(Fall|Spring)");
        });

        itParam("return them there should be removed ${value.Semester} keep: ${value.Keep}", template, function({Semester, Keep}){
            let remove = semester.getOldSemesters([{Semester}], new Date(2019, 8, 15));
            remove.length.should.be.equal(1-Keep);
        });

    })
});