const chai = require('chai'),
    chaiHttp = require('chai-http'),
    config = require("../config");
var itParam = require('mocha-param');
var MockDate = require('mockdate');

var expect = chai.expect;
const Joi = require('@hapi/joi');


chai.use(chaiHttp);

chai.should();

config.port = false;
let app = require("../server");
let semester = require("../helpers/semester")

const schema = Joi.object().keys({
    Id : Joi.string().required(),
    CourseName : Joi.string().required(),
    Room : Joi.string().required(),
    TeacherId : Joi.number().required(),
    Students: Joi.array().items(Joi.number()).required(),
    Category : Joi.string().required(),
    NoHoursPrWeek : Joi.number().required(),
    Weekdays : Joi.string().required(),
    Description : Joi.string().required(),
    Semester : Joi.string().required(),
    Language : Joi.string().required(),
    Level : Joi.string().required(),
    Visible : Joi.boolean().required(),
    Archived : Joi.boolean().required(),

});


describe("Course", function () {
    it("Get all", async function () {
        let data = await chai.request(app).get("/courses").send();

        data.status.should.be.equal(200)
        const result = Joi.validate(data.body, Joi.array().items(schema));
        expect(result.error).to.be.null;
        expect(data.body.length).to.be.least(2)
    });

    it("Get all with less students than", async function () {
        let threeStudents = await chai.request(app).post("/courses").send({
            CourseName : "Test",
            Room : "Test room",
            TeacherId : 0,
            Students : [1,2,3],
            Category: "Testing",
            NoHoursPrWeek : 5,
            Weekdays: "Mon",
            Description: "A course on testing",
            Semester: "2019_Fall",
            Language: "English",
            Level: "Easy",
            Visible: true,
            Archived : false
        });

        expect(threeStudents).to.have.status(200);

        let fourStudents = await chai.request(app).post("/courses").send({
            CourseName : "Test",
            Room : "Test room",
            TeacherId : 0,
            Students : [1,2,3, 4],
            Category: "Testing",
            NoHoursPrWeek : 5,
            Weekdays: "Mon",
            Description: "A course on testing",
            Semester: "2019_Fall",
            Language: "English",
            Level: "Easy",
            Visible: true,
            Archived : false
        });

        expect(fourStudents).to.have.status(200);


        let data = await chai.request(app).get("/courses?maximumstudents=3").send();

        data.status.should.be.equal(200)

        expect(data.body.find(i => i.Id === threeStudents.body.Id)).to.be.a("object")
        expect(data.body.find(i => i.Id === fourStudents.body.Id)).to.be.undefined;
    });

    it("Get all where max is a string", async function () {
        let data = await chai.request(app).get("/courses?maximumstudents=four").send();

        expect(data).to.have.status(400);
        expect(data.body.error).to.be.a("string")
    });

    describe("Get a single course", function () {
        it("Success", async function () {
            let all = await chai.request(app).get("/courses").send();
            let firstCourse = all.body[0];

            let data = await chai.request(app).get("/courses/"+firstCourse.Id).send();
            data.body.should.be.eql(firstCourse);
            expect(data).to.have.status(200);


        });

        it("Invalid id", async function () {
            let data = await chai.request(app).get("/courses/0").send();

            expect(data).to.have.status(400);
        });


        it("Not found", async function () {
            let data = await chai.request(app).get("/courses/000000000000000000000000").send();

            expect(data).to.have.status(404);
            data.body.error.should.be.eql("Not found");
        });
    });

    describe("Insert", function () {
        it("Success", async function () {
            let data = {
                CourseName : "Test",
                Room : "Test room",
                TeacherId : 0,
                Students : [1,2,3],
                Category: "Testing",
                NoHoursPrWeek : 5,
                Weekdays: "Mon",
                Description: "A course on testing",
                Semester: "2019_Fall",
                Language: "English",
                Level: "Easy",
                Visible: true,
                Archived : false
            };
            let req = await chai.request(app).post("/courses").send(data);

            expect(req).to.have.status(200);
            expect(req.body.Id).to.be.a("string");

            data.Id = req.body.Id;

            expect(req.body).to.eql(data);
        });
        it("Failes", async function () {
            let data = {
                CourseName : ["Not", "Right"],
                Room : "Test room",
                TeacherId : 0,
                Students : [1,2,3],
                Category: "Testing",
                NoHoursPrWeek : 5,
                Weekdays: "Mon",
                Description: "A course on testing",
                Semester: "2019_Fall",
                Language: "English",
                Level: "Easy",
                Visible: true,
                Archived : false
            };
            let req = await chai.request(app).post("/courses").send(data);

            expect(req).to.have.status(400);
            expect(req.body.error).to.be.a("string")
        });

    });

    describe("Deactivates a course", function () {
        it("Successes", async function () {
            let all = await chai.request(app).get("/courses").send();
            let firstCourse = all.body.find(i => i.Archived === false);

            expect(firstCourse).to.be.a("object");


            let deactivateReq = await chai.request(app).delete("/courses/" + firstCourse.Id).send();
            expect(deactivateReq).to.have.status(202);

            let data = await chai.request(app).get("/courses/"+firstCourse.Id).send();
            expect(data).to.have.status(200);
            expect(data.body.Archived).to.be.true;


        });

        it("Invalid id", async function () {
            let data = await chai.request(app).delete("/courses/0").send();

            expect(data).to.have.status(400);
        });


        it("Not found", async function () {
            let data = await chai.request(app).delete("/courses/000000000000000000000000").send();

            expect(data).to.have.status(404);
            data.body.error.should.be.eql("Not found");
        });
    });

    describe("Edit course", function(){
        it("Updates", async function(){
            let all = await chai.request(app).get("/courses").send();
            let firstCourse = all.body[0];

            expect(firstCourse).to.be.a("object");


            let update = await chai.request(app).patch("/courses/"+firstCourse.Id).send({
                CourseName: firstCourse.CourseName+"edit",
                Room: firstCourse.Room+"edit",
                TeacherId: firstCourse.TeacherId+1,
                Students: [...firstCourse.Students, 1],
                Category: firstCourse.Category+"edit",
                NoHoursPrWeek: firstCourse.NoHoursPrWeek+1,
                Weekdays: firstCourse.Weekdays+"edit",
                Description: firstCourse.Description+"edit",
                Semester: "1999_Fall",
                Language: firstCourse.Language+"edit",
                Level: firstCourse.Level+"edit",
                Visible: !firstCourse.Visible,
                Archived: !firstCourse.Archived,
            });
            expect(update).to.have.status(202);


            let data = await chai.request(app).get("/courses/"+firstCourse.Id).send();

            expect(data.body.Id).to.eql(firstCourse.Id);
            expect(data.body.CourseName).to.eql(firstCourse.CourseName+"edit");
            expect(data.body.Room).to.eql(firstCourse.Room+"edit");
            expect(data.body.TeacherId).to.eql(firstCourse.TeacherId+1);
            expect(data.body.Students).to.eql([...firstCourse.Students, 1]);
            expect(data.body.Category).to.eql(firstCourse.Category+"edit");
            expect(data.body.NoHoursPrWeek).to.eql(firstCourse.NoHoursPrWeek+1);
            expect(data.body.Weekdays).to.eql(firstCourse.Weekdays+"edit");
            expect(data.body.Description).to.eql(firstCourse.Description+"edit");
            expect(data.body.Semester).to.eql("1999_Fall");
            expect(data.body.Language).to.eql(firstCourse.Language+"edit");
            expect(data.body.Level).to.eql(firstCourse.Level+"edit");
            expect(data.body.Visible).to.eql(!firstCourse.Visible);
            expect(data.body.Archived).to.eql(!firstCourse.Archived)


            let updateAgain = await chai.request(app).patch("/courses/"+data.body.Id).send({
                CourseName: data.body.CourseName+"edit",
                Room: data.body.Room+"edit",
                TeacherId: data.body.TeacherId+1,
                Students: [...data.body.Students, 1],
                Category: data.body.Category+"edit",
                NoHoursPrWeek: data.body.NoHoursPrWeek+1,
                Weekdays: data.body.Weekdays+"edit",
                Description: data.body.Description+"edit",
                Semester: "1999_Fall",
                Language: data.body.Language+"edit",
                Level: data.body.Level+"edit",
                Visible: !data.body.Visible,
                Archived: !data.body.Archived,
            });
            expect(updateAgain).to.have.status(202);


            let newdata = await chai.request(app).get("/courses/"+firstCourse.Id).send();

            expect(newdata.body.Id).to.eql(firstCourse.Id);
            expect(newdata.body.CourseName).to.eql(firstCourse.CourseName+"editedit");
            expect(newdata.body.Room).to.eql(firstCourse.Room+"editedit");
            expect(newdata.body.TeacherId).to.eql(firstCourse.TeacherId+2);
            expect(newdata.body.Students).to.eql([...firstCourse.Students, 1, 1]);
            expect(newdata.body.Category).to.eql(firstCourse.Category+"editedit");
            expect(newdata.body.NoHoursPrWeek).to.eql(firstCourse.NoHoursPrWeek+2);
            expect(newdata.body.Weekdays).to.eql(firstCourse.Weekdays+"editedit");
            expect(newdata.body.Description).to.eql(firstCourse.Description+"editedit");
            expect(newdata.body.Semester).to.eql("1999_Fall");
            expect(newdata.body.Language).to.eql(firstCourse.Language+"editedit");
            expect(newdata.body.Level).to.eql(firstCourse.Level+"editedit");
            expect(newdata.body.Visible).to.eql(firstCourse.Visible);
            expect(newdata.body.Archived).to.eql(firstCourse.Archived)
        });

        it("Updates with id", async function(){
            let all = await chai.request(app).get("/courses").send();
            let firstCourse = all.body[0];

            expect(firstCourse).to.be.a("object");

            let newStuds = Array.from(firstCourse.Students);
            newStuds.push(1);
            let update = await chai.request(app).patch("/courses/"+firstCourse.Id).send({
                Id: firstCourse.Id,
                CourseName: firstCourse.CourseName+"edit",
                Room: firstCourse.Room+"edit",
                TeacherId: firstCourse.TeacherId+1,
                Students: [1],
                Category: firstCourse.Category+"edit",
                NoHoursPrWeek: firstCourse.NoHoursPrWeek+1,
                Weekdays: firstCourse.Weekdays+"edit",
                Description: firstCourse.Description+"edit",
                Semester: "1999_Fall",
                Language: firstCourse.Language+"edit",
                Level: firstCourse.Level+"edit",
                Visible: !firstCourse.Visible,
                Archived: !firstCourse.Archived,
            });
            expect(update).to.have.status(202);



            let data = await chai.request(app).get("/courses/"+firstCourse.Id).send();

            firstCourse.Students.push(1);
            expect(data.body.Id).to.eql(firstCourse.Id);
            expect(data.body.CourseName).to.eql(firstCourse.CourseName+"edit");
            expect(data.body.Room).to.eql(firstCourse.Room+"edit");
            expect(data.body.TeacherId).to.eql(firstCourse.TeacherId+1);
            expect(data.body.Students).to.eql([1]);
            expect(data.body.Category).to.eql(firstCourse.Category+"edit");
            expect(data.body.NoHoursPrWeek).to.eql(firstCourse.NoHoursPrWeek+1);
            expect(data.body.Weekdays).to.eql(firstCourse.Weekdays+"edit");
            expect(data.body.Description).to.eql(firstCourse.Description+"edit");
            expect(data.body.Semester).to.eql("1999_Fall");
            expect(data.body.Language).to.eql(firstCourse.Language+"edit");
            expect(data.body.Level).to.eql(firstCourse.Level+"edit");
            expect(data.body.Visible).to.eql(!firstCourse.Visible);
            expect(data.body.Archived).to.eql(!firstCourse.Archived)
        });

        it("Fails - wrong id", async function(){
            let all = await chai.request(app).get("/courses").send();
            let firstCourse = all.body[0];

            expect(firstCourse).to.be.a("object");

            let newStuds = Array.from(firstCourse.Students);
            newStuds.push(1);
            let data = await chai.request(app).patch("/courses/"+firstCourse.Id).send({
                Id:firstCourse.Id+"24432",
                CourseName: firstCourse.CourseName+"edit",
                Room: firstCourse.Room+"edit",
                TeacherId: firstCourse.TeacherId+1,
                Students: newStuds,
                Category: firstCourse.Category+"edit",
                NoHoursPrWeek: firstCourse.NoHoursPrWeek+1,
                Weekdays: firstCourse.Weekdays+"edit",
                Description: firstCourse.Description+"edit",
                Semester: "1999_Fall",
                Language: firstCourse.Language+"edit",
                Level: firstCourse.Level+"edit",
                Visible: !firstCourse.Visible,
                Archived: !firstCourse.Archived,
            });
            expect(data).to.have.status(400);
            expect(data.body.error).to.be.contain("You cant update the id")

            
        });

        it("Failes - malformated", async function () {
            let all = await chai.request(app).get("/courses").send();
            let firstCourse = all.body[0];

            expect(firstCourse).to.be.a("object");


            let data = await chai.request(app).patch("/courses/"+firstCourse.Id).send({
                CourseName: ["Not", "Right"],
                Room: "Test room",
                TeacherId: 0,
                Students: [1, 2, 3],
                Category: "Testing",
                NoHoursPrWeek: 5,
                Weekdays: "Mon",
                Description: "A course on testing",
                Semester: "2019_Fall",
                Language: "English",
                Level: "Easy",
                Visible: true,
                Archived: false
            });

            expect(data).to.have.status(400);
            expect(data.body.error).to.be.a("string")
        });

        it("Failes - to many fields", async function () {
            let all = await chai.request(app).get("/courses").send();
            let firstCourse = all.body[0];

            expect(firstCourse).to.be.a("object");


            let data = await chai.request(app).patch("/courses/"+firstCourse.Id).send({
                CourseName: "Test",
                NotOK: "Hehe",
                Room: "Test room",
                TeacherId: 0,
                Students: [1, 2, 3],
                Category: "Testing",
                NoHoursPrWeek: 5,
                Weekdays: "Mon",
                Description: "A course on testing",
                Semester: "2019_Fall",
                Language: "English",
                Level: "Easy",
                Visible: true,
                Archived: false
            });

            expect(data).to.have.status(400);
            expect(data.body.error).to.be.a("string");
            expect(data.body.error).to.be.contain("NotOK")
        });

        it("Invalid id", async function () {
            let data = await chai.request(app).patch("/courses/0").send();

            expect(data).to.have.status(400);
        });


        it("Not found", async function () {
            let data = await chai.request(app).patch("/courses/000000000000000000000000").send({
                CourseName : "Test",
                Room : "Test room",
                TeacherId : 0,
                Students : [1,2,3],
                Category: "Testing",
                NoHoursPrWeek : 5,
                Weekdays: "Mon",
                Description: "A course on testing",
                Semester: "2019_Fall",
                Language: "English",
                Level: "Easy",
                Visible: true,
                Archived : false
            });

            expect(data).to.have.status(404);
            data.body.error.should.be.eql("Not found");
        });
    })


    describe('Delete archive', function (){
        let baseDate = new Date(2019, 8, 15);

        it("removes it from database", async function(){
            let shouldBeDeleted = await chai.request(app).post("/courses").send({
                CourseName : "Test",
                Room : "Test room",
                TeacherId : 0,
                Students : [1,2,3],
                Category: "Testing",
                NoHoursPrWeek : 5,
                Weekdays: "Mon",
                Description: "A course on testing",
                Semester: "2016_Fall",
                Language: "English",
                Level: "Easy",
                Visible: true,
                Archived : false
            });

            expect(shouldBeDeleted).to.have.status(200);

            let shouldBeKept = await chai.request(app).post("/courses").send({
                CourseName : "Test",
                Room : "Test room",
                TeacherId : 0,
                Students : [1,2,3, 4],
                Category: "Testing",
                NoHoursPrWeek : 5,
                Weekdays: "Mon",
                Description: "A course on testing",
                Semester: "2017_Spring",
                Language: "English",
                Level: "Easy",
                Visible: true,
                Archived : false
            });

            expect(shouldBeKept).to.have.status(200);

            MockDate.set(baseDate);
            let deleteRequest = await chai.request(app).delete("/courses/clean_archive").send();
            console.log(deleteRequest.body)
            deleteRequest.status.should.be.equal(200)
            expect(deleteRequest.body.nodel).to.be.a("Number")
            deleteRequest.body.nodel.should.be.least(1)
            MockDate.reset();

            let data = await chai.request(app).get("/courses").send();

            data.status.should.be.equal(200)

            expect(data.body.find(i => i.Id === shouldBeKept.body.Id)).to.be.a("object")
            expect(data.body.find(i => i.Id === shouldBeDeleted.body.Id)).to.be.undefined;


            MockDate.reset();


        })


        it("fails to remove it from database", async function(){
            let shouldBeDeleted = await chai.request(app).post("/courses").send({
                CourseName : "Test",
                Room : "Test room",
                TeacherId : 0,
                Students : [1,2,3],
                Category: "Testing",
                NoHoursPrWeek : 5,
                Weekdays: "Mon",
                Description: "A course on testing",
                Semester: "2012Spring",
                Language: "English",
                Level: "Easy",
                Visible: true,
                Archived : false
            });

            expect(shouldBeDeleted).to.have.status(200);

            MockDate.set(baseDate);
            let deleteRequest = await chai.request(app).delete("/courses/clean_archive").send();
            deleteRequest.status.should.be.equal(500);
            expect(deleteRequest.body.error).to.be.a("String");
            MockDate.reset();


        })



    })
});