const mongoose = require('mongoose');
let express = require("express");

const Joi = require('@hapi/joi');
const ejoi = require("./../middleware/ejoi");

require('../mongoose');


let app = express();


const hasMongoId = ejoi(Joi.object().keys({
    id: Joi.string().length(24).required()
}), "params");

app.post("/", ejoi(Joi.object().keys({
    CourseIds: Joi.array().items(Joi.string().length(24)).required()
}), "body"), async (req, res) => {
    let model = new (mongoose.model("Votes"))({
        CourseIds: req.body.CourseIds,
        Active: true
    })
    await model.save();

    for(let id of req.body.CourseIds){
        let model = await mongoose.model("Course").findById(id)
        if(model === null) continue;
        if(model.Votes != 0){
            model.Votes = 0;
            await model.save();
        }
    }
    
    res.json({Id: model._id})
});

app.get("/", async (req, res) => {
    let data = await mongoose.model("Votes").aggregate([{
        $project: {
            _id: 0,
            Id: '$_id',
            Active: '$Active',
            CreatedAt: '$CreatedAt'
        }
    }]);
    res.json(data)
});



app.get('/:id', hasMongoId, async (req, res) => {
    let data = await mongoose.model("Votes").findById(req.params.id)
        .populate({path: 'CourseIds', select: "_id CourseName Votes Category"});
   
    if (data == null) {
        return res.status(404).json({error: "Not found"})
    }  
    data = data.toObject();

    data.Id = data._id;
    delete data._id;
    delete data.__v;
    data.Courses = data.CourseIds.map(i => {
        i.Id = i._id;
        delete i._id;
        i.Name = i.CourseName;
        delete i.CourseName;
        return i;
    })
    delete data.CourseIds;
    res.json(data);
});

app.post('/addVotes', ejoi(Joi.object().keys({
    CourseIds: Joi.array().items(Joi.string().length(24)).required()
}), "body"), async (req, res) => {
    for(let id of req.body.CourseIds){
        let model = await mongoose.model("Course").findById(id)
        if(model === null) continue;
        model.Votes = parseInt(model.Votes) + 1;
        await model.save();
    }
    res.status(202).end();
})

app.patch('/:id/disable', hasMongoId, async (req, res) => {
    let model = await mongoose.model("Votes").findByIdAndUpdate(req.params.id, {Active: false})
    if (model == null) {
        return res.status(404).json({error: "Not found"})
    }  
    res.status(202).end();

})

module.exports = app;
