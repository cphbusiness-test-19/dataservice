const mongoose = require('mongoose');
let express = require("express");

const Joi = require('@hapi/joi');
const ejoi = require("./../middleware/ejoi");

require('../mongoose');
let semester = require("../helpers/semester")


let app = express();

let fields = {
    _id: 0,
    Id: '$_id',
    CourseName: '$CourseName',
    Room: '$Room',
    TeacherId: '$TeacherId',
    Students: '$StudentsId',
    Category: '$Category',
    NoHoursPrWeek: '$NoHoursPrWeek',
    Weekdays: '$Weekdays',
    Description: '$Description',
    Semester: '$Semester',
    Language: '$Language',
    Level: '$Level',
    Visible: '$Visible',
    Archived: '$Archived',
    Votes: '$Votes'
};


const validateCourse = {
    Id: Joi.string(),
    CourseName: Joi.string().allow("").required(),
    Room: Joi.string().allow("").required(),
    TeacherId: Joi.number().required(),
    Students: Joi.array().items(Joi.number()).required(),
    Category: Joi.string().allow("").required(),
    NoHoursPrWeek: Joi.number().required(),
    Weekdays: Joi.string().allow("").required(),
    Description: Joi.string().allow("").required(),
    Semester: Joi.string().allow("").required(),
    Language: Joi.string().allow("").required(),
    Level: Joi.string().allow("").required(),
    Visible: Joi.boolean().required(),
    Archived: Joi.boolean().required(),
};


const hasMongoId = ejoi(Joi.object().keys({
    id: Joi.string().length(24).required()
}), "params");


app.get('/', ejoi(Joi.object().keys({
    maximumstudents: Joi.number().min(0)
}), "query"), async (req, res) => {
    if (req.query.maximumstudents) {
        //filters.push({$expr:{$lte:[{$size:"$Students"}, req.query.maximumstudents]})
        let data = await mongoose.model("Course").aggregate([{
            $project: {...fields, size_of_studentes: {$size: "$StudentsId"}},
        }, {
            $match: {"size_of_studentes": {$lte: parseInt(req.query.maximumstudents)}}
        }, {
            $project: {size_of_studentes: 0}
        }]);
        data = data.map(i => {
            delete i.Votes;
            return i
        })
        res.json(data)
    } else {
        let data = await mongoose.model("Course").aggregate([{
            $project: fields
        }]);
        data = data.map(i => {
            delete i.Votes;
            return i
        })
        res.json(data)
    }
});


app.get('/:id', hasMongoId, async (req, res) => {
    let data = await mongoose.model("Course").aggregate([
        {
            $match: {
                _id: mongoose.Types.ObjectId(req.params.id)
            }
        }, {
            $project: fields
        }]);
    if (data.length !== 1) {
        return res.status(404).json({error: "Not found"})
    }
    delete data[0].Votes;
    res.json(data[0]);
});


app.post("/", ejoi(Joi.object().keys(validateCourse), "body"), async (req, res) => {
    let model = new (mongoose.model("Course"))({
        CourseName: req.body.CourseName,
        Room: req.body.Room,
        TeacherId: req.body.TeacherId,
        StudentsId: req.body.Students,
        Category: req.body.Category,
        NoHoursPrWeek: req.body.NoHoursPrWeek,
        Weekdays: req.body.Weekdays,
        Description: req.body.Description,
        Semester: req.body.Semester,
        Language: req.body.Language,
        Level: req.body.Level,
        Visible: req.body.Visible,
        Archived: req.body.Archived,
        Votes: 0
    });
    await model.save();

    let modelObject = model.toObject();
    modelObject.Id = modelObject._id;
    modelObject.Students = modelObject.StudentsId;
    delete modelObject._id;
    delete modelObject.__v;
    delete modelObject.StudentsId;
    delete modelObject.Votes;
    res.json(modelObject);
});

app.patch("/:id", hasMongoId, ejoi(validateCourse, "body"), async (req, res) => {
    if (typeof req.body.Id !== "undefined") {
        if (req.body.Id !== req.params.id) {
            return res.status(400).json({error: "You cant update the id"})
        }
        delete req.body.Id;
    }
    if (await mongoose.model("Course").findById(req.params.id) === null) {
        return res.status(404).json({error: "Not found"})
    }

    let data = await mongoose.model("Course").findById(req.params.id);
    await data.remove();

    let model = new (mongoose.model("Course"))({
        _id: mongoose.Types.ObjectId(req.params.id),
        CourseName: req.body.CourseName,
        Room: req.body.Room,
        TeacherId: req.body.TeacherId,
        StudentsId: req.body.Students,
        Category: req.body.Category,
        NoHoursPrWeek: req.body.NoHoursPrWeek,
        Weekdays: req.body.Weekdays,
        Description: req.body.Description,
        Semester: req.body.Semester,
        Language: req.body.Language,
        Level: req.body.Level,
        Visible: req.body.Visible,
        Archived: req.body.Archived,
        Votes: data.Votes
    });
    await model.save();


    return res.status(202).end();
});

app.delete("/clean_archive", async (req, res) => {
    try{
        let allCourses = await mongoose.model("Course").find({Archived: 0});
        let getOld = semester.getOldSemesters(allCourses);

        await Promise.all(getOld.map(i => i.remove()))

        res.status(200).json({nodel: getOld.length});

    }catch(e){
        res.status(500).json({error: e.message})
    }
})

app.delete("/:id", hasMongoId, async (req, res) => {
    let model = await mongoose.model("Course").findById(req.params.id);
    if (model === null) {
        return res.status(404).json({error: "Not found"})
    }
    model.Archived = true;
    await model.save();
    res.status(202).end();
});

module.exports = app;
