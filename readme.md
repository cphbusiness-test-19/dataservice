sudo docker run -p 80:80 -e DATABASEURL="mongodb://mongo/les" --link dbms:mongo --name dataservices dataservices



<details>
<summary><b>Course</b></summary>
<details>
<summary>GET /courses</summary>

Request variables: `{
    ?maximumstudents: number
}`

The maximumstudents is 0 and up, send back only courses that have upto and including the maximumstudents-number of students enrolled.

Response structure
```
[{
    Id : string
    CourseName : string
    Room : string
    TeacherId : number
    Students : [number]
    Category : string
    NoHoursPrWeek : number
    Weekdays : string
    Description : string
    Semester : string
    Language : string
    Level : string
    Visible : bool
    Archived : bool
}]
```
</details>
<details>
<summary>GET /courses/:id</summary>

Request variables: `{}`

Response structure - 200
```
{
    Id : string
    CourseName : string
    Room : string
    TeacherId : number
    Students : [number]
    Category : string
    NoHoursPrWeek : number
    Weekdays : string
    Description : string
    Semester : string
    Language : string
    Level : string
    Visible : bool
    Archived : bool
}
```

Response structure - 400 - invalid id
```
{
    error: "Id not 24 char long"
}
```

Response structure - 404 - not found
```
{
    error: "Not found"
}
```


</details>
<details>
<summary>POST /courses</summary>

Request variables:
```
{
    CourseName : string
    Room : string
    TeacherId : number
    Students : [number]
    Category : string
    NoHoursPrWeek : number
    Weekdays : string
    Description : string
    Semester : string
    Language : string
    Level : string
    Visible : bool
    Archived : bool
}
```

Response structure - 200
```
{
    Id : string
    CourseName : string
    Room : string
    TeacherId : number
    Students : [number]
    Category : string
    NoHoursPrWeek : number
    Weekdays : string
    Description : string
    Semester : string
    Language : string
    Level : string
    Visible : bool
    Archived : bool
}
```

Response structure - 400 - invalid data
```
{
    error: string
}
```

</details>

<details>
<summary>PATCH /courses/:id</summary>

Request variables:
```
{
    ?Id: string (has to be the same as the one in the url which is required)
    CourseName : string
    Room : string
    TeacherId : number
    Students : [number]
    Category : string
    NoHoursPrWeek : number
    Weekdays : string
    Description : string
    Semester : string
    Language : string
    Level : string
    Visible : bool
    Archived : bool
}
```

Response structure - 202 - ok

Response structure - 400 - invalid data
```
{
    error: string
}
```


Response structure - 404 - not found
```
{
    error: "Not found"
}
```


</details>


<details>
<summary>DELETE /courses/:id</summary>

Request variables:
```
{
    
}
```

Response structure - 202
```
```

Response structure - 400 - invalid id
```
{
    error: "Id not 24 char long"
}
```

Response structure - 404 - not found
```
{
    error: "Not found"
}
```



</details>

<details>
<summary>DELETE courses/clean_archive</summary>

Request variables:
```
{
    
}
```

Response structure - 200
```
{
 nodel : number
}
```

Response structure - 500
```
{
 error : "A course did not have the right semester format"
}
```




</details>

</details>
<details>
<summary><b>Vote</b></summary>

<details>
<summary>POST /vote - new voting</summary>

Request variables:
```
{
    CourseIds : [list with mongo ids]
}
```

Response structure - 200
```
{
    Id: string
}
```

Response structure - 500
```
{
    error: "CourseIds not an array of strings 24 char long"
}
```
</details>



<details>
<summary>GET /vote - all active and deactivated votes</summary>

Request variables:
```
{
    
}
```

Response structure - 200
```
[{
	Id: mongo id
	Aktiv: bool
	CreatedAt: date
}]
```
</details>


<details>
<summary>GET /vote:mongoId</summary>

Request variables:
```
{
    
}
```

Response structure - 200
```
[{
	Id: mongo id
	Aktiv: bool
	CreatedAt: date
	Courses : [{
		Id: mongo id
		Name: string
		Votes: int
		Category: string
	}]
}]
```

Response structure - 400 - invalid id
```
{
    error: "Id not 24 char long"
}
```

Response structure - 404 - not found
```
{
    error: "Not found"
}
```

</details>



<details>
<summary>PATCH /vote/:mongoId/disable</summary>

Request variables:
```
{
    
}
```


Response structure - 200 - success
```
{
}
```


Response structure - 400 - invalid id
```
{
    error: "Id not 24 char long"
}
```

Response structure - 404 - not found
```
{
    error: "Not found"
}
```

</details>



<details>
<summary>POST /vote/addVotes</summary>

Request variables:
```
{
    CourseIds : [list with mongo ids]
}
```

Response structure - 200
```
{
}
```


Response structure - 500
```
{
    error: "CourseIds not an array of strings 24 char long"
}
```
</details>


</details>