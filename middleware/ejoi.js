const Joi = require('@hapi/joi');

function ejoi(validationConstraints, prop) {
    return function (req, res, next) {
        let validate = Joi.validate(req[prop], validationConstraints);
        if (validate.error === null) {
            next()
        } else {
            res.status(400).json({error: validate.error.message})
        }
    };
}

module.exports = ejoi;